void main() {
  var output =
      application("wumdrop", "fleet management", "Simon Hartley", 2015);
  output.printMyOutput();
  output.capitalLetter();
}

class application {
  String? app;
  String? category;
  String? developer;
  int? yearWon;

  application(this.app, this.category, this.developer, this.yearWon);

  void printMyOutput() {
    print("App name: $app");
    print("App category is: $category");
    print("App was developed by: $developer");
    print("App won in: $yearWon");
  }

  void capitalLetter() {
    String appName = "wumdrop";
    print(appName.toUpperCase());
  }
}
